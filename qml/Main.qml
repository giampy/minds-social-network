/*
 * Copyright (C) 2020  Giampietro D'Antonio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ubuntu-calculator-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import QtQml 2.0
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import Ubuntu.Components 1.3
import Ubuntu.UnityWebApps 0.1 as UnityWebApps
import QtWebEngine 1.7
import QtSystemInfo 5.5
import Morph.Web 0.1

MainView {
  id:root
  objectName: "root"
  theme.name: "Ubuntu.Components.Themes.Ambiance"
  applicationName: "minds.giampy"
  anchorToKeyboard: true
  automaticOrientation: true

  width: units.gu(45)
  height: units.gu(75)

    WebEngineView {
      id: webview
      anchors {
        fill: parent
      }
      property var currentWebview: webview
      settings.pluginsEnabled: true
      settings.showScrollBars: false

      profile:  WebEngineProfile {
        id: oxideContext
        property alias dataPath: oxideContext.persistentStoragePath

        httpUserAgent: "Mozilla/5.0 (Linux; Ubuntu 16.04 like Android 4.4) AppleWebKit/537.36 Chrome/77.0.3865.129 Mobile Safari/537.36"
        offTheRecord: false
        ataPath: dataLocation
        persistentCookiesPolicy: WebEngineProfile.ForcePersistentCookies
      }

      //zoomFactor: 2.5
      url: "https://www.minds.com"
      
    }

    Connections {
      target: Qt.inputMethod
      onVisibleChanged: nav.visible = !nav.visible
    }
}